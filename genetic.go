package main

import (
	"fmt"
	"math"
	"math/rand"
	"strings"
	"sync"
	"time"
)

// Genetic stores the state of the genetic algorithm
type Genetic struct {
	depth       int
	length      int
	popSize     int
	measureBits []int
	inExpr      *string
	pop         [][]Gate
	popErr      []float32
}

// NewGenetic creates a new instance of the Genetic struct
func NewGenetic(depth, length, popSize int, measureBits []int, inExpr *string) *Genetic {
	return &Genetic{
		depth:       depth,
		length:      length,
		popSize:     popSize,
		measureBits: measureBits,
		inExpr:      inExpr,
	}
}

// train trains the genetic algorithm for the specified amount of rounds, with the given inputs and outputs
func (g *Genetic) train(rounds int, inputs [][][]float64, outputs []map[string]int) {
	for i := 0; i < rounds; i++ {
		for j := 0; j < len(inputs); j++ {
			g.testPops(inputs[j], outputs[j])
			g.sortPops()
			g.destroyPop()
			g.mutateReproduce()
		}
	}
}

// genPop creates a new population
func (g *Genetic) genPop() {
	// Create mutex to protect population slice
	var popMtx sync.Mutex
	// Create population slice initialized with popSize items
	population := make([][]Gate, g.popSize)

	// Create new waitgroup
	wg := sync.WaitGroup{}
	for i := 0; i < g.popSize; i++ {
		wg.Add(1)
		go func(i int) {
			// Signal goroutine completion at end
			defer wg.Done()
			// Create new population member
			newAlgo := make([]Gate, g.length)

			// Initialize expression index as -1
			exprIndex := -1
			// If expression is provided
			if g.inExpr != nil {
				// Create new random number
				randLoc := rand.Intn(g.length)
				// Add expression to random index
				newAlgo[randLoc] = Gate{
					Expr: g.inExpr,
				}
				// Set expression index to location
				exprIndex = randLoc
			}

			// Loop g.length times
			for j := 0; j < g.length; j++ {
				// If index is an expression, skip
				if exprIndex == j {
					continue
				}

				// Create new random number
				randInt := rand.Intn(NumGateType)
				// Create new random number
				index := rand.Intn(g.depth)

				// Get gate corresponding to first random number
				gate := getGate(randInt, index, g.depth)

				// Set member to new gate
				newAlgo[j] = gate
			}
			// Lock population slice
			popMtx.Lock()
			// Set population member in population
			population[i] = newAlgo
			// Unlock population slice
			popMtx.Unlock()
		}(i)
	}
	// Wait for all goroutines to complete
	wg.Wait()
	// Set population in struct
	g.pop = population
}

// genCode generates OpenQASM code for a population
func (g *Genetic) genCode(inputs [][]float64, all bool) []string {
	// Create output slice
	var out []string
	// For every member in population
	for index, gates := range g.pop {
		// If all is false and index is greater than 0, stop loop
		if !all && index > 0 {
			break
		}

		// Create new string builder for QASM
		qasmStrBuilder := strings.Builder{}

		// For every input
		for initIndex, input := range inputs {
			// Initialize using y rotation
			qasmStrBuilder.WriteString(fmt.Sprintf("ry(%f) q[%d];\n",
				2*math.Acos(input[0]),
				initIndex,
			))
		}

		// For every gate
		for _, gate := range gates {
			// If gate contains expression
			if gate.Expr != nil {
				// Write expression unaltered
				qasmStrBuilder.WriteString(*gate.Expr)
				// Skip to next loop
				continue
			}
			// If gate type is empty, skip
			if gate.Type == "" {
				continue
			}
			// Write gate type to builder
			qasmStrBuilder.WriteString(gate.Type)

			// For every index in gate
			for i, gateIndex := range gate.Indices {
				// Determine prefix for qreg index
				prefix := " "
				if i > 0 {
					prefix = ","
				}
				// Write index with prefix to builder
				qasmStrBuilder.WriteString(fmt.Sprintf(prefix+"q[%d]", gateIndex))
			}
			// Write semicolon with newline to builder
			qasmStrBuilder.WriteString(";\n")
		}
		// For every measured bit
		for _, val := range g.measureBits {
			// Expand MeasureTmpl and write to builder
			qasmStrBuilder.WriteString(fmt.Sprintf(MeasureTmpl, val, val))
		}
		// Append expanded QasmTmpl to out slice
		out = append(out, fmt.Sprintf(QasmTmpl,
			g.depth,
			g.depth,
			qasmStrBuilder.String(),
		))
	}

	return out
}

// testPops tests how accurate a population is
func (g *Genetic) testPops(input [][]float64, outputs map[string]int) {
	// Create error slice initialized with population size
	g.popErr = make([]float32, g.popSize)

	// Run code and retrieve results
	results := runQasmPython(g.genCode(input, true))

	// For every result
	for i, result := range results {
		// Create counter for correctness
		var correctCounter float32 = 0
		// For every output
		for key, output := range outputs {
			// Attempt to get corresponding result
			val, ok := result[key]
			// If result exists
			if ok {
				if output > val {
					correctCounter += float32(val)
				} else {
					correctCounter += float32(output)
				}
			}
		}
		// Add error to struct
		g.popErr[i] = correctCounter / 1024
	}
}

// sortPops uses the merge sort algorithm to sort error along with the population
func (g *Genetic) sortPops() {
	g.popErr, g.pop = mergeSort(g.popErr, g.pop)
	fmt.Println("Best error:", g.popErr[0])
}

// destroyPop removes 20% of the population
func (g *Genetic) destroyPop() {
	topindex := int(float32(len(g.pop)) * 0.2)
	g.pop = g.pop[:topindex]
	g.popErr = g.popErr[:topindex]
	g.popSize = topindex
}

// Define mutation types
const (
	MutateTypeRand = iota
	MutateTypeChangeQubit
	MutateTypeSwap
)
const NumMutateType = 3

// mutateReproduce reproduces the population and then mutates it
func (g *Genetic) mutateReproduce() {
	// Copy population
	top := g.pop
	// Add population to itself 4 times
	for i := 0; i < 4; i++ {
		g.pop = append(g.pop, top...)
	}

	// Multiply population size by 5
	g.popSize *= 5
	// Get index of top 10% of population and loop starting from there
	topIndex := int(float32(len(g.pop)) * 0.1)
	for i := topIndex; i < len(g.pop); i++ {
		// For every gate in population member
		for j := range g.pop[i] {
			// If random number with limit of 10 is less than 2 (20% chance)
			if rand.Intn(10) < 2 {
				// Create new random number for mutation type selection
				typeofMutate := rand.Intn(NumMutateType)
				switch typeofMutate {
				case MutateTypeRand:
					// Create new random gate
					newGate := getGate(rand.Intn(NumGateType), j, g.depth)
					// Set expression if it exists (expressions should remain unmodified)
					newGate.Expr = g.pop[i][j].Expr
					// Set new gate
					g.pop[i][j] = newGate
				case MutateTypeChangeQubit:
					// If gate has indices (not expression)
					if len(g.pop[i][j].Indices) > 0 {
						// Create new random number for new index
						newIndex := rand.Intn(g.depth)
						// For every index after the first
						for _, index := range g.pop[i][j].Indices[1:] {
							// While new index equals index
							for newIndex == index {
								// Sleep 1ms (time based numbers)
								time.Sleep(time.Millisecond)
								// Create new random number and try again
								newIndex = rand.Intn(g.depth)
							}
						}
						// Set new index
						g.pop[i][j].Indices[0] = newIndex
					}
				case MutateTypeSwap:
					// Create new random number for the index of the gate to be swapped
					swapIndex := rand.Intn(g.length)
					// Swap gates
					g.pop[i][j], g.pop[i][swapIndex] = g.pop[i][swapIndex], g.pop[i][j]
				}
			}
		}
	}
}
