package main

import (
	"encoding/json"
	"log"
	"os"
	"os/exec"
	"strings"
	"sync"
)

// runQasmPython runs generated OpenQASM circuits using qiskit via Python
func runQasmPython(qasmAlgos []string) []map[string]int {
	// Make slice of maps to store results of running generated code
	results := make([]map[string]int, len(qasmAlgos))
	// Create new waitgroup
	wg := sync.WaitGroup{}
	for index, qasmAlgo := range qasmAlgos {
		// Create new command executing `qasm.py` as a python file
		cmd := exec.Command("python", "qasm.py")
		// Feed quantum algorithm to Stdin
		cmd.Stdin = strings.NewReader(qasmAlgo)
		cmd.Stderr = os.Stderr
		// Add goroutine to waitgroup
		wg.Add(1)
		go func(index int) {
			// Get command output
			data, _ := cmd.Output()
			// Create map to store results
			var resultMap map[string]int
			// Unmarshal JSON from output
			err := json.Unmarshal(data, &resultMap)
			if err != nil {
				log.Fatalln(err)
			}
			// Add results map to slice
			results[index] = resultMap
			// Signal goroutine completion
			wg.Done()
		}(index)

	}
	// Wait for all goroutines to complete
	wg.Wait()
	return results
}

// mergeSort implements part of the merge sort sorting algorithm
func mergeSort(items []float32, sideitems [][]Gate) ([]float32, [][]Gate) {
	var num = len(items)

	if num == 1 {
		return items, sideitems
	}

	middle := num / 2

	var (
		left      = make([]float32, middle)
		right     = make([]float32, num-middle)
		sideleft  = make([][]Gate, middle)
		sideright = make([][]Gate, num-middle)
	)

	for i := 0; i < num; i++ {
		if i < middle {
			left[i] = items[i]
			sideleft[i] = sideitems[i]
		} else {
			right[i-middle] = items[i]
			sideright[i-middle] = sideitems[i]
		}
	}

	recurseOne, recurseTwo := mergeSort(left, sideleft)
	recurseThree, recurseFour := mergeSort(right, sideright)
	outputOne, outputTwo := merge(recurseOne, recurseThree, recurseTwo, recurseFour)
	return outputOne, outputTwo
}

// merge implements part of the merge sort sorting algorithm
func merge(left, right []float32, sideleft, sideright [][]Gate) (result []float32, sideresult [][]Gate) {
	result = make([]float32, len(left)+len(right))
	sideresult = make([][]Gate, len(left)+len(right))

	i := 0
	for len(left) > 0 && len(right) > 0 {
		if left[0] < right[0] {
			result[i] = left[0]
			sideresult[i] = sideleft[0]
			left = left[1:]
			sideleft = sideleft[1:]
		} else {
			result[i] = right[0]
			sideresult[i] = sideright[0]
			right = right[1:]
			sideright = sideright[1:]
		}
		i++
	}

	for j := 0; j < len(left); j++ {
		result[i] = left[j]
		sideresult[i] = sideleft[j]
		i++
	}
	for j := 0; j < len(right); j++ {
		result[i] = right[j]
		sideresult[i] = sideright[j]
		i++
	}

	return
}
