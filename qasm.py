import qiskit
import sys
import json

# Read from Stdin
qasmStr = sys.stdin.read()
# Use input to create a qiskit circuit
qasmCircuit = qiskit.QuantumCircuit.from_qasm_str(qasmStr)
# Get Aer simulator and set device to GPU
backend = qiskit.Aer.get_backend("aer_simulator")
backend.set_options(device="GPU")
# Execute circuit in simulator
result = qiskit.execute(qasmCircuit, backend).result()
# Output the results from the simulator as JSON
print(json.dumps(result.get_counts(qasmCircuit)))
