package main

// QasmTmpl is a template for an OpenQASM program
const QasmTmpl = `OPENQASM 2.0;
include "qelib1.inc";

qreg q[%d];
creg c[%d];

%s`

// MeasureTmpl is a template for measurements in OpenQASM
const MeasureTmpl = `measure q[%d] -> c[%d];`
