package main

import (
	"flag"
	"fmt"
	"math"
	"math/rand"
	"time"
)

func init() {
	// Seed random number generator with time to precision of nanoseconds
	rand.Seed(time.Now().UnixNano())
}

func main() {
	// Register and parse flags
	expr := flag.String("expr", "", "Expression to add to quantum algorithm")
	flag.Parse()

	// Set variable to nil if expression not provided
	var inExpr *string
	if *expr != "" {
		inExpr = expr
	} else {
		inExpr = nil
	}

	// Create test input
	input := [][][]float64{
		{{1 / math.Sqrt(2), 1 / math.Sqrt(2)}},
		{{math.Sqrt(1. / 3), math.Sqrt(2. / 3)}},
	}

	// Create test output
	output := []map[string]int{
		{"01": 512, "00": 512},
		{"01": 307, "00": 717},
	}

	// Create new Genetic struct
	g := NewGenetic(2, 2, 10, []int{0}, inExpr)
	// Run genetic algorithm and get best performing code
	bestCode, popErr := runAlgorithm(g, input, output)
	// Print a newline
	fmt.Print("\n")
	// Print best performing OpenQASM code
	fmt.Println(bestCode)
	fmt.Println()
	fmt.Println(popErr)
}

// runAlgorithm starts the training of the genetic algorithm and returns the results
func runAlgorithm(g *Genetic, input [][][]float64, output []map[string]int) (string, []float32) {
	// Create new population
	g.genPop()
	// Train population
	g.train(3, input, output)
	// Generate code for best performing member
	bestCode := g.genCode(input[0], false)
	return bestCode[0], g.popErr
}
