package main

import (
	"log"
	"math/rand"
	"time"
)

// Gate stores quantum gates
type Gate struct {
	Type    string
	Indices []int
	Expr    *string
}

// Define gate types
const (
	GateTypeH = iota
	GateTypeNot
	GateTypeCNot
	GateTypeToffoli
	GateTypeI
)
const NumGateType = 4

// getGate gets a gate corresponding to the input
func getGate(inputInt, index, depth int) Gate {
	switch inputInt {
	case GateTypeH:
		return Gate{
			Type:    "h",
			Indices: []int{index},
		}
	case GateTypeNot:
		return Gate{
			Type:    "x",
			Indices: []int{index},
		}
	case GateTypeCNot:
		if depth == 1 {
			// Sleep 1ms (time based numbers)
			time.Sleep(time.Millisecond)
			// Create new random number
			newInput := rand.Intn(NumGateType)
			// Attempt to get different gate
			return getGate(newInput, index, depth)
		}
		// Create new random number
		control := rand.Intn(depth)
		for control == index {
			// Sleep 1ms (time based numbers)
			time.Sleep(time.Millisecond)
			// Create new random number
			control = rand.Intn(depth)
		}
		return Gate{
			Type:    "cx",
			Indices: []int{index, control},
		}
	case GateTypeToffoli:
		if depth == 1 || depth == 2 {
			// Sleep 1ms (time based numbers)
			time.Sleep(time.Millisecond)
			// Create new random number
			newInput := rand.Intn(NumGateType)
			// Attempt to get different gate
			return getGate(newInput, index, depth)
		}
		// Create new random number
		indexOne := rand.Intn(depth)
		for indexOne == index {
			// Sleep 1ms (time based numbers)
			time.Sleep(time.Millisecond)
			// Create new random number
			indexOne = rand.Intn(depth)
		}
		// Create new random number
		indexTwo := rand.Intn(depth)
		for indexTwo == index || indexTwo == indexOne {
			// Sleep 1ms (time based numbers)
			time.Sleep(time.Millisecond)
			// Create new random number
			indexTwo = rand.Intn(depth)
		}
		return Gate{
			Type:    "ccx",
			Indices: []int{index, indexOne, indexTwo},
		}
	case GateTypeI:
		return Gate{
			Type:    "id",
			Indices: []int{index},
		}
	default:
		log.Fatalln("Unknown gate number:", inputInt)
	}
	return Gate{}
}
